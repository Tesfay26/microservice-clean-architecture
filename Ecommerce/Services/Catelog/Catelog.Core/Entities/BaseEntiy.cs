﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Catelog.Core.Entities
{
    public class BaseEntiy
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
    }
}
