﻿using MongoDB.Bson.Serialization.Attributes;

namespace Catelog.Core.Entities
{
    public class ProductType : BaseEntiy
    {
        [BsonElement("Name")]
        public string Name { get; set; }
    }
}
