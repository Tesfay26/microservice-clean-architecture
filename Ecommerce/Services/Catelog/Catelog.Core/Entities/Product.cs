﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Catelog.Core.Entities
{
    public class Product : BaseEntiy
    {
        //[BsonId]
        //[BsonRepresentation(BsonType.ObjectId)]
        //public string Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        public String Summary { get; set; }
        public string Description { get; set; }
        public string ImageFile { get; set; }
        public ProductBrand Brand { get; set; }
        public ProductType Type { get; set; }

        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Price { get; set; }
    }
}
