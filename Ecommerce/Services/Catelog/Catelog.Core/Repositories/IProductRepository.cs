﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catelog.Core.Entities;
using Catelog.Core.Specs;

namespace Catelog.Core.Repositories
{
    public interface IProductRepository
    {
        Task<Pagination<Product>> GetProducts(CatalogSpecParams catalogSpecParams);
        Task<Product> GetProduct(String id);
        Task<IEnumerable<Product>> GetProductsByName(String name);
        Task<IEnumerable<Product>> GetProductsByBrand(String brandName);
        Task<Product> CreateProduct(Product product);
        Task<bool> UpdateProduct(Product product);
        Task<bool> DeleteProduct(String id);
    }
}
