﻿using Catelog.Application.Mappers;
using Catelog.Application.Responses;
using Catelog.Core.Entities;
using Catelog.Core.Repositories;
using MediatR;

namespace Catelog.Application.Commands
{
    public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, bool>
    {
        private readonly IProductRepository _productRepository;

        public UpdateProductCommandHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public async Task<bool> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            var existedProduct = await _productRepository.GetProduct(request.Id);
            if (existedProduct is null)
            {
                throw new ApplicationException($"There is no any product with {request.Id} id.");
            }
            
            var updatedProduct = await _productRepository.UpdateProduct(new Product
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description,
                Summary = request.Summary,
                ImageFile = request.ImageFile,
                Brand = request.Brand,
                Type = request.Type,
                Price = request.Price,
            });
            return true;
        }
    }
}
