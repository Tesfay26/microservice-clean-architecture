﻿using AutoMapper;
using Catelog.Application.Commands;
using Catelog.Application.Responses;
using Catelog.Core.Entities;
using Catelog.Core.Specs;

namespace Catelog.Application.Mappers
{
    public class ProductMappingProfile : Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<ProductBrand, BrandsResponse>().ReverseMap();
            CreateMap<Product, ProductsResponse>().ReverseMap();
            CreateMap<ProductType, TypesResponse>().ReverseMap();
            CreateMap<Product, CreateProductCommand>().ReverseMap();
            CreateMap<Pagination<Product>, Pagination<ProductsResponse>>().ReverseMap();
        }
    }
}
