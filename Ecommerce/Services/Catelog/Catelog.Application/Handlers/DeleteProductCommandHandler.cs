﻿using Catelog.Application.Commands;
using Catelog.Core.Repositories;
using MediatR;

namespace Catelog.Application.Handlers
{
    public class DeleteProductCommandHandler : IRequestHandler<DeleteProductCommand, bool>
    {
        private readonly IProductRepository _productRepository;

        public DeleteProductCommandHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public Task<bool> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            return _productRepository.DeleteProduct(request.Id);
        }
    }
}
