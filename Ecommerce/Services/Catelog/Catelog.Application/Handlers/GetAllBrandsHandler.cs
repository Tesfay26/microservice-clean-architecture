﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Catelog.Application.Mappers;
using Catelog.Application.Queries;
using Catelog.Application.Responses;
using Catelog.Core.Entities;
using Catelog.Core.Repositories;
using MediatR;

namespace Catelog.Application.Handlers
{
    public class GetAllBrandsHandler : IRequestHandler<GetAllBrandsQuery, IList<BrandsResponse>>
    {
        //private readonly IMapper _mapper;
        private readonly IBrandRepository _brandRepository;
        public GetAllBrandsHandler(IBrandRepository brandRepository)
        {
            _brandRepository = brandRepository;
            //_mapper = mapper;
        }
        public async Task<IList<BrandsResponse>> Handle(GetAllBrandsQuery request, CancellationToken cancellationToken)
        {
            var brandList = await _brandRepository.GetAllBrands();
            //var brandResponseList = _mapper.Map<IList<BrandResponse>>(brandList);
            //var brandResponseList = _mapper.Map<IList<ProductBrand>, IList<BrandResponse>>(brandList.ToList());
            var brandResponseList = ProductMapper.Mapper.Map<IList<ProductBrand>, IList<BrandsResponse>>(brandList.ToList());
            return brandResponseList;
        }
    }
}
