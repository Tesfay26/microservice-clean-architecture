﻿using Catelog.Application.Mappers;
using Catelog.Application.Queries;
using Catelog.Application.Responses;
using Catelog.Core.Repositories;
using MediatR;

namespace Catelog.Application.Handlers
{
    public class GetAllTypeHandler : IRequestHandler<GetAllTypeQuery, IList<TypesResponse>>
    {
        private readonly ITypesRepository _typeRepository;
        public GetAllTypeHandler(ITypesRepository typesRepository)
        {
            _typeRepository = typesRepository;
        }
        public async Task<IList<TypesResponse>> Handle(GetAllTypeQuery request, CancellationToken cancellationToken)
        {
            var typeList = await _typeRepository.GetAllProductTypes();
            var typeListResponse = ProductMapper.Mapper.Map<IList<TypesResponse>>(typeList);  
            return typeListResponse;
        }
    }
}
