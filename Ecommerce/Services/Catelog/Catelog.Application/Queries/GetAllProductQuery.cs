﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catelog.Application.Responses;
using Catelog.Core.Specs;
using MediatR;

namespace Catelog.Application.Queries
{
    public class GetAllProductQuery : IRequest<Pagination<ProductsResponse>>
    {
        public CatalogSpecParams CatalogSpecParams { get; set; }

        public GetAllProductQuery(CatalogSpecParams catalogSpecParams)
        {
            CatalogSpecParams = catalogSpecParams;
        }

    }
}
