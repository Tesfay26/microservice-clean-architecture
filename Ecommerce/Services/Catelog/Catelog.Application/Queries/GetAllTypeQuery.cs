﻿using Catelog.Application.Responses;
using MediatR;

namespace Catelog.Application.Queries
{
    public class GetAllTypeQuery : IRequest<IList<TypesResponse>>
    {
    }
}
