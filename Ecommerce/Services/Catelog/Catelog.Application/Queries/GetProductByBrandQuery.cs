﻿using Catelog.Application.Responses;
using MediatR;

namespace Catelog.Application.Queries
{
    public class GetProductByBrandQuery : IRequest<IList<ProductsResponse>>
    {
        public string BrandName { get; set; }
        public GetProductByBrandQuery(string brandName)
        {
            BrandName = brandName;
        }
    }
}
