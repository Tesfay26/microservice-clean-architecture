﻿using Catelog.Application.Responses;
using MediatR;

namespace Catelog.Application.Queries
{
    public class GetAllBrandsQuery : IRequest<IList<BrandsResponse>>
    {
    }
}
