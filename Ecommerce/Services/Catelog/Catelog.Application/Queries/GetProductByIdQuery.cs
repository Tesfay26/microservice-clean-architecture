﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catelog.Application.Responses;
using MediatR;

namespace Catelog.Application.Queries
{
    public class GetProductByIdQuery : IRequest<ProductsResponse>
    {
        public string Id { get; set; }
    }
}
