﻿using Catelog.Application.Responses;
using MediatR;

namespace Catelog.Application.Queries
{
    public class GetProductByNameQuery : IRequest<IList<ProductsResponse>>
    {
        public string Name { get; set; }
        public GetProductByNameQuery(string name)
        {
            Name = name;
        }
    }
}
