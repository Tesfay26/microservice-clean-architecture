﻿using System.Net;
using Catelog.Application.Commands;
using Catelog.Application.Queries;
using Catelog.Application.Responses;
using Catelog.Core.Specs;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.HttpSys;

namespace Catelog.API.Controllers
{
    public class CatalogController : ApiController
    {
        private readonly IMediator _mediator;

        public CatalogController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("GetAllProducts", Name = "GetAllProducts")]
        [ProducesResponseType(typeof(Pagination<ProductsResponse>), 200)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<Pagination<ProductsResponse>>> GetAllProducts([FromQuery]CatalogSpecParams catalogSpecParams)
        {
            var query = new GetAllProductQuery(catalogSpecParams);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("[action]/{productId}", Name = "GetProductById")]
        [ProducesResponseType(typeof(ProductsResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<ProductsResponse>> GetProductById(string productId)
        {
            var query = new GetProductByIdQuery { Id = productId };
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("[action]/{productName}", Name = "GetProductByProductName")]
        [ProducesResponseType(typeof(IList<ProductsResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<IList<ProductsResponse>>> GetProductByProductName(string productName)
        {
            var query = new GetProductByNameQuery(productName);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllBrands", Name = "GetAllBrands")]
        [ProducesResponseType(typeof(IList<BrandsResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<IList<ProductsResponse>>> GetAllBrands()
        {
            var query = new GetAllBrandsQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllTypes", Name = "GetAllTypes")]
        [ProducesResponseType(typeof(IList<TypesResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<IList<ProductsResponse>>> GetAllTypes()
        {
            var query = new GetAllTypeQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet]
        [Route("[action]/{brandName}", Name = "GetProductByBrandName")]
        [ProducesResponseType(typeof(IList<ProductsResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<IList<ProductsResponse>>> GetProductByBrandName(string brandName)
        {
            var query = new GetProductByBrandQuery(brandName);
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpPost]
        [Route("CreateProduct")]
        [ProducesResponseType(typeof(ProductsResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ProductsResponse>> CreateProduct([FromBody] CreateProductCommand command)
        {
            var result = await _mediator.Send<ProductsResponse>(command);
            return Ok(result);
        }

        [HttpPut]
        [Route("UpdateProduct")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> UpdateProduct([FromBody] UpdateProductCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete]
        [Route("{productId}", Name = "DeleteProduct")]
        [ProducesResponseType(typeof (bool), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> DeleteProduct(string productId)
        {
            var command = new DeleteProductCommand(productId);
            var result = await _mediator.Send(command);
            return Ok(result);
        }
    }
}
