﻿using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;

namespace Catelog.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ApiController : ControllerBase
    {
    }
}
