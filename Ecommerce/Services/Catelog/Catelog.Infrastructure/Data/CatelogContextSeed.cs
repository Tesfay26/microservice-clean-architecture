﻿using System.Text.Json;
using Catelog.Core.Entities;
using MongoDB.Driver;

namespace Catelog.Infrastructure.Data
{
    public static class CatelogContextSeed
    {
        public static void SeedData(IMongoCollection<Product> productCollection)
        {
            var checkProducts = productCollection.Find(b => true).Any();
            string path = Path.Combine("Data", "SeedData", "products.json");
            if (!checkProducts)
            {
                var productData = File.ReadAllText(path);
                var products = JsonSerializer.Deserialize<List<Product>>(productData);
                if (products != null)
                {
                    foreach (var item in products)
                    {
                        productCollection.InsertOneAsync(item);
                    }
                }
            }
        }
    }
}
