﻿using System.Linq;
using Catelog.Core.Entities;
using Catelog.Core.Repositories;
using Catelog.Core.Specs;
using Catelog.Infrastructure.Data;
using MongoDB.Driver;

namespace Catelog.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository, IBrandRepository, ITypesRepository
    {
        public ICatelogContext _context { get; }
        public ProductRepository(ICatelogContext catelogContext)
        {
            _context = catelogContext;
        }

        public async Task<Pagination<Product>> GetProducts(CatalogSpecParams catalogSpecParams)
        {
            var builder = Builders<Product>.Filter;
            var filter = builder.Empty;
            if (!string.IsNullOrEmpty(catalogSpecParams.Search))
            {
                filter = filter & builder.Where(p => p.Name.ToLower().Contains(catalogSpecParams.Search.ToLower()));
            }
            if (!string.IsNullOrEmpty(catalogSpecParams.BrandId)) { 
               var brandFilter = builder.Eq(p => p.Brand.Id, catalogSpecParams.BrandId);
                filter &= brandFilter;
            }
            if(!string.IsNullOrEmpty(catalogSpecParams.TypeId))
            {
                var typeFilter = builder.Eq(q => q.Type.Id, catalogSpecParams.TypeId);
                filter &= typeFilter;
            }

            var totalItems = await _context.Products.CountDocumentsAsync(filter);
            var data = await DataFilter(catalogSpecParams, filter);
            //var data = await _context.Products
            //    .Find(filter)
            //    .Skip((catalogSpecParams.PageIndex - 1) * catalogSpecParams.PageSize)
            //    .Limit(catalogSpecParams.PageSize)
            //    .ToListAsync();

            return new Pagination<Product>(
                 catalogSpecParams.PageIndex,
                 catalogSpecParams.PageSize,
                 (int)totalItems,
                 data
                );

            //return await _context.Products.Find(p=>true).ToListAsync();
        }

        private async Task<IReadOnlyList<Product>> DataFilter(CatalogSpecParams catalogSpecParams, FilterDefinition<Product> filter)
        {
            var sortDefn = Builders<Product>.Sort.Ascending("Name"); //Default
            if (!string.IsNullOrEmpty(catalogSpecParams.Sort))
            {
                switch(catalogSpecParams.Sort)
                {
                    case "priceAsc":
                        sortDefn = Builders<Product>.Sort.Ascending(p => p.Price); 
                        break;
                    case "priceDesc":
                        sortDefn = Builders<Product>.Sort.Descending(p => p.Price);
                        break;
                    default:
                        sortDefn = Builders<Product>.Sort.Ascending(p => p.Name);
                        break;
                }
            }

            return await _context
                .Products
                .Find(filter)
                .Sort(sortDefn)
                .Skip(catalogSpecParams.PageSize * (catalogSpecParams.PageIndex - 1))
                .Limit(catalogSpecParams.PageSize)
                .ToListAsync();
        }

        public async Task<Product> GetProduct(string id)
        {
            return await _context.Products.Find(p=>p.Id == id).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<Product>> GetProductsByBrand(string brandnName)
        {
            return await _context.Products.Find(p=>p.Brand.Name.ToLower() == brandnName.ToLower()).ToListAsync();
        }

        public async Task<IEnumerable<Product>> GetProductsByName(string name)
        {
            return await _context.Products.Find(p=>p.Name.ToLower() == name.ToLower()).ToListAsync();
        }
        public async Task<Product> CreateProduct(Product product)
        {
            await _context.Products.InsertOneAsync(product);
            return product;
        }

        public async Task<bool> DeleteProduct(string id)
        {
            var deleteProduct = await _context.Products.DeleteOneAsync(p => p.Id == id);
            return deleteProduct.IsAcknowledged && deleteProduct.DeletedCount > 0;
        }

        public async Task<bool> UpdateProduct(Product product)
        {
            var updateProduct = await _context.Products.ReplaceOneAsync(p=>p.Id == product.Id, product);
            return updateProduct.IsAcknowledged && updateProduct.ModifiedCount > 0;
        }

        public async Task<IEnumerable<ProductBrand>> GetAllBrands()
        {
            return await _context.Brands.Find(p=> true).ToListAsync();
        }

        public async Task<IEnumerable<ProductType>> GetAllProductTypes()
        {
           return await _context.Types.Find(p=>true).ToListAsync();
        }
    }
}
